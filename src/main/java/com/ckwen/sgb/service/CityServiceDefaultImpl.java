package com.ckwen.sgb.service;

import org.springframework.stereotype.Service;

/**
 * @author wen
 */
@Service
public class CityServiceDefaultImpl implements CityService {

    @Override
    public String getName() {
        return "未知城市";
    }
}
