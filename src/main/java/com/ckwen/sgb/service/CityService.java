package com.ckwen.sgb.service;

/**
 * @author wen
 */
public interface CityService {

    /**
     * 返回城市名
     * @return
     */
    String getName();
}
