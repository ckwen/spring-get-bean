package com.ckwen.sgb.web;

import com.ckwen.sgb.service.CityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * @author wen
 */


@Slf4j
@RestController
public class HomeController {

    private static List<String> cityList = Arrays.asList("0411", "0755");

    @Autowired
    private ApplicationContext applicationContext;

    @GetMapping
    public String home() {

        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        Arrays.sort(beanDefinitionNames);
        for (String beanDefinitionName : beanDefinitionNames) {
            log.info(beanDefinitionName);
        }

        return "Hello";
    }

    @GetMapping("{code}")
    public String print(@PathVariable String code) {

        String beanName = "cityServiceDefaultImpl";

        if (StringUtils.hasLength(code) && cityList.contains(code)) {
            beanName = "cityService" + code + "Impl";
        }

        CityService cityService = applicationContext.getBean(beanName, CityService.class);

        return cityService.getName();
    }

}
