package com.ckwen.sgb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author wen
 */
@SpringBootApplication
public class SpringGetBeanApplication {

	public static void main(String[] args) {

		// SpringApplication.run(SpringGetBeanApplication.class, args);

		ConfigurableApplicationContext applicationContext = SpringApplication.run(SpringGetBeanApplication.class, args);

		String port = applicationContext.getEnvironment().getProperty("server.port");

		String path = applicationContext.getEnvironment().getProperty("server.servlet.context-path");

		String localUrl = "http://localhost:" + port + path;

		System.out.println("启动成功，访问地址：" + localUrl);
	}

}
